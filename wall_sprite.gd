extends Node3D

@onready var raycast = get_parent()
@onready var raycast_owner = get_parent().get_owner()

@onready var wall = preload("res://wall.tscn")

var snap = Vector3()

func _ready():
	self.top_level = true
	self.global_transform.origin = raycast.get_collision_point()
	snap.x = snapped(raycast.get_collision_point().x + raycast.get_collision_normal().x / 10, 1)
	snap.y = snapped(raycast.get_collision_point().y + raycast.get_collision_normal().y / 10, 1)
	snap.z = snapped(raycast.get_collision_point().z + raycast.get_collision_normal().z / 10, 1)
	self.global_transform.origin = snap

func _input(_event):
	if Input.is_action_just_pressed("fire"):
		var w = wall.instantiate() as Node3D
		#w.golbal_transform.origin = self.global_transform.origin
		#w.set_global_position(self.get_global_position())
		get_tree().get_root().add_child(w)
		snap.x = snapped(raycast.get_collision_point().x + raycast.get_collision_normal().x / 10, 1)
		snap.y = snapped(raycast.get_collision_point().y + raycast.get_collision_normal().y / 10, 1)
		snap.z = snapped(raycast.get_collision_point().z + raycast.get_collision_normal().z / 10, 1)
		w.global_transform.origin = snap
		#self.queue_free()

func _process(_delta):
	self.global_transform.origin = raycast.get_collision_point()
	snap.x = snapped(raycast.get_collision_point().x + raycast.get_collision_normal().x / 10, 1)
	snap.y = snapped(raycast.get_collision_point().y + raycast.get_collision_normal().y / 10, 1)
	snap.z = snapped(raycast.get_collision_point().z + raycast.get_collision_normal().z / 10, 1)
	self.global_transform.origin = snap
	self.rotation = raycast_owner.rotation
