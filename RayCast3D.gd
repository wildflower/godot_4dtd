extends RayCast3D

@onready var wall_sprite = preload("res://wall_sprite.tscn")

func _input(_event):
	if Input.is_key_pressed(KEY_E):
		if self.get_child_count() == 0:
			var ws = wall_sprite.instantiate()
			add_child(ws)
		else:
			get_child(0).queue_free()
